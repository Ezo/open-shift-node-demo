const express = require('express');
const app = express();
const expressRouter = express.Router();

expressRouter.get('/', (req, res) => res.json(new Date()));
app.use(expressRouter);
app.listen(3000, () => console.log('Started!'));

